import requests
from pprint import pprint
import flask
import flask_sqlalchemy
from sqlalchemy.dialects.postgresql import JSON
import urllib
import time
import wikipedia

app = flask.Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:recycle123@throwresponsibly.me:5432/postgres'
db = flask_sqlalchemy.SQLAlchemy(app)

class Item(db.Model):
    id =            db.Column(db.Integer,primary_key=True)
    name =          db.Column(db.String(255))
    family_ids=      db.Column(JSON)
    long_desc=      db.Column(db.String(255))
    avg_price=      db.Column(db.String(255))
    listings=       db.Column(JSON)
    img_url=        db.Column(db.String(255))
    score=          db.Column(db.Float)
    definition=     db.Column(db.String(255))

class Program(db.Model):
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    program_type=   db.Column(db.String(255))
    city=           db.Column(db.String(255))
    materials=      db.Column(JSON)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))
db.create_all()

FAMILIES_URL = "https://api.earth911.com/earth911.getFamilies?api_key=10e91611cc8c3d96"

MATERIALS_URL = "http://api.earth911.com/earth911.getMaterials?api_key=10e91611cc8c3d96"

# API limit 3/sec 3k per month
IMAGE_URL="https://api.cognitive.microsoft.com/bing/v7.0/images/search"
IMAGE_HEADER = {
'Ocp-Apim-Subscription-Key': '5781912b472246c0aa8b6bc898b1fbcb'
}

EBAY_URL = "https://api.ebay.com/buy/browse/v1/item_summary/search?q="
EBAY_HEADER = {
    'Authorization' : "Bearer v^1.1#i^1#I^3#p^1#r^0#f^0#t^H4sIAAAAAAAAAOVXe2wURRjv9UVqqcagFUsjx7b4B7i7s7v3XLmDa0vpyaOVK68SQuZ2Z9u1d7uXnTmuVxPTlFINJCJoRExM8B/FkIiE0EgIBCOIGmpifaFBo6LBR1SiQQ0x4uzdUa6VAIVDSNzkcrfffPPN7/d9v2/mBvSVV8wabBn8o8oxqXhHH+grdjiESlBRXjb79pLimrIikOfg2NFX31faX/LdHAzjsYS8FOGEaWDk7InHDCxnjAEmaRmyCbGOZQPGEZaJIkdCixfJIgfkhGUSUzFjjDPcFGD8mtvrdktev0uFyOdyUatxIWa7GWBE1S0CryQij0/RooKXjmOcRGEDE2gQOg4EHysAVvS1AyCLPlnyc5IgdDDO5cjCumlQFw4wwQxcOTPXysN6eagQY2QRGoQJhkPNkdZQuGn+kvY5fF6sYC4PEQJJEo99azRV5FwOY0l0+WVwxluOJBUFYczwwewKY4PKoQtgrgF+JtWCpCnQTx8v9Eoel1iQVDabVhySy+OwLbrKahlXGRlEJ+krZZRmI/oIUkjubQkNEW5y2l8PJ2FM13RkBZj5DaFVobY2JtjeZZkpKht29Efb0iYWeTUXEBVJZJEoqUjwuXILZaPl0jxupUbTUHU7adi5xCQNiKJG43Mj5OWGOrUarVZIIzaifD/xQg6Bp8MuaraKSdJl2HVFcZoIZ+b1yhUYnU2IpUeTBI1GGD+QSVGAgYmErjLjBzNazMmnBweYLkISMs+nUikuJXGm1cmLAAj8ysWLIkoXikOG+tq9nvXXrzyB1TNUFERnYl0m6QTF0kO1SgEYnUxQoo9XzOV9LKzgeOu/DHmc+bEdUagOcSNREQBUkBdEhSgsSIcEcyLlbRwoCtNsHFrdiCRidB1WoTpLxpGlq7Lk1kTJpyFW9fg11uXXNDbqVj2soCEEEIpGFb/v/9QoVyv1iGImUJsZ05V0QQRfOLFbahu0SDqCYjFquFrVX5IktknecHp2r0+Ioh0D0yAwoXO2tjnFjPMmpJuabVqbQX1dvEOJRDgeTxIYjaFwYTa0m7SZXZKeTo/7W4oTrV+2kLqaPae5TDU5vE7hLITNpEX/onCt9rHVbnYjg24CxDJjMWQtF6670LdYfSe4V14b78Id1BPkTXtdvKHaVmI6ldDam8PuJldVh+TWYi24XcDj87gF/3XxaszUtD39H5xFE6LXYmKC1Bvwv5Ife8sNFmUeod+xD/Q79tCLMuDBTKEOzCgvWVZaMrkG6wRxOtQ4rHca9PJmIa4bpRNQt4rLHatrX3tlbd69escaMHX0Zl1RIlTmXbNB7cWRMuGOe6oEnwBEH6Afyd8B6i6OlgrVpXdx588d3DAytGBaoKV31rrqbav6eg+DqlEnh6OsqLTfUXT81K69O6VN/S1TNi49/ermAfjiwELI73J5fziys+HIys/ujprF/H3rhyf9VL116MDAVzufOfb9E6/PeWH3n+9sf/kDa/PRuaeO/fzgmukdQ96zuvDLsyseHZp2OjUyb8HHqeFV8+d9uelc80D1VP6po+k9UNlWMz21/qP363f/Xjdl4a9g8icjRY8devL8sf2Db+1bvP7Dl1Ldn74xc1lV05ot7F8HZLXp/m17v9hw4hT7efvZGSfnpXxvbn+us7Zm9jR+N9k4svp4y50rmKcf2l/S+3fKMB+fW3fbsP7egR9R5cHhk/UjzbPqz3wbPHTv13rDsriKTwz2vv1uY8UDz5/pqW3bdfibLZUbftOdU9Vs+f4Buh6VcfEQAAA="
}

already_calculated = []
def average_price(name):
    name = name.replace('#','')
    augmented_items = {}
    urlencoded_query = urllib.parse.quote(name)
    rurl = EBAY_URL+urlencoded_query+"&limit=5"
    ebay_search_result = requests.get(rurl, headers=EBAY_HEADER)
    if ebay_search_result.status_code == 200:
        output = ebay_search_result.json()
        if 'itemSummaries' in output:
            item_summaries = output['itemSummaries']
            titles = []
            image_urls = []
            item_web_url = []
            count = 0
            price= 0
            for summary in item_summaries:
                if "price" in summary:
                     count += 1
                     price += float(summary["price"]["value"])
                if "title" in summary:
                    titles.append(summary["title"])
                if "image" in summary:
                    image = summary["image"]
                    if "imageUrl" in image:
                        image_urls.append(image["imageUrl"])
                if "itemWebUrl" in summary:
                    item_web_url.append(summary["itemWebUrl"])

            augmented_items = {"listings": {"titles":titles, "image_urls":image_urls, "item_web_url":item_web_url},"price": price/count }

    return augmented_items

def calculate_score(items):
    sum = 0.0
    for item in items:
        if item in family_counts:
            sum += family_counts[item]
    return sum

def get_image_url(name):
    urlencoded_query = urllib.parse.quote(name)
    rurl = IMAGE_URL + "?" + "q=" + urlencoded_query

    image_search_result = requests.get(rurl, headers=IMAGE_HEADER)
    if image_search_result.status_code == 200:
        j = image_search_result.json()
        # j['value'][0]['thumbnailUrl'] # lower quality image
        val = j['value']
        if len(val) > 0:
            return j['value'][0]['contentUrl']
    return ""

def screenvals(d):
    for key in d:
        if(d[key] == None or d[key] == "" or d[key] == {}):
            return False
    return True

#355 items
def scrape_items():
    count = 0
    r = requests.get(MATERIALS_URL)
    if r.status_code == 200:
        json = r.json()
        result = json['result']
        for item in result:
            if item['material_id'] not in already_calculated:
                # print(item['description'], end=" ")
                # print(item['family_ids'][0], end=" ")
                # print(item['material_id'])
                listings = {}
                ebay = average_price(item['description'])
                if 'listings' in ebay:
                    listings = ebay['listings']
                if 'price' in ebay:
                    price = ebay['price']
                else:
                    price = 0.0

                defin = "N/A"
                try:
                    defin = wikipedia.summary(item['description'],sentences=1)
                except Exception as e:
                    pass

                if 'family_ids' in item:
                    output = {
                        'name' : item['description'],
                        'family_ids' : item['family_ids'],
                        'desc' : item['long_description'],
                        'id' : item['material_id'],
                        'avg_price' : price,
                        'ebay' : listings,
                        'score' : calculate_score(item['family_ids']),
                        'image_url' : get_image_url(item['description']),
                        'definition': defin[0:250]
                    }
                    # print(screenvals(output))
                    time.sleep(1) # sleep to avoid image api limit

                    if screenvals(output):
                        count+=1
                        item = Item(id = output['id'],
                                        name = output['name'],
                                        family_ids= output['family_ids'],
                                        long_desc= output['desc'],
                                        avg_price= output['avg_price'],
                                        listings=  output['ebay'],
                                        img_url=   output['image_url'],
                                        score= output['score'],
                                        definition=output['definition'])
                        db.session.add(item)
                        db.session.commit()
    print(count)

material_counts = dict()
family_counts = dict()
score_max_val = 0
score_min_val = 100000000

def calculate_static_scorevals():
    for instance in db.session.query(Program, Program.materials):
        json = instance.materials
        for elem in json:
            id = (elem['id'])
            if id not in material_counts:
                material_counts[id] = 0
            material_counts[id] += 1

    family_dict = dict()
    r = requests.get(FAMILIES_URL)
    if(r.status_code == 200):
        result = r.json()['result']
        for family in result:
            if 'material_ids' in family:
                if family['family_id'] not in family_dict:
                    family_dict[family['family_id']] = []
                for material in family['material_ids']:
                    family_dict[family['family_id']] += [material]

        for family in family_dict:
            sum = 0.0
            count = 0
            for elem in family_dict[family]:
                if elem in material_counts:
                    count += 1
                    sum += material_counts[elem]
            family_counts[family] = sum

        del family_counts[108]
        del family_counts[106]
        # pprint(family_counts)
    l = list(material_counts.values())
    length = len(l)
    l.sort()
    score_min_val = l[0]
    score_max_val = l[length-1]
    # print(score_min_val, '-', score_max_val)

    for key in material_counts:
        material_counts[key] = material_counts[key]/score_max_val*100

if __name__ == '__main__':
    calculate_static_scorevals()
    for instance in db.session.query(Item, Item.id):
        already_calculated.append(instance.id)
    scrape_items()
