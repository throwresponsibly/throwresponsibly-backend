import requests
from pprint import pprint
import flask
import flask_sqlalchemy
from sqlalchemy.dialects.postgresql import JSON
import urllib
import time

app = flask.Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:recycle123@throwresponsibly.me:5432/postgres'
db = flask_sqlalchemy.SQLAlchemy(app)

class Facility(db.Model):
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    facility_type=  db.Column(db.String(255))
    city=           db.Column(db.String(255))
    materials=      db.Column(JSON)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))

class TempFacility(db.Model):
    id =            db.Column(db.String(255),primary_key=True)
    lat =           db.Column(db.Float())
    lng =           db.Column(db.Float())
    state =         db.Column(db.String(255))

db.create_all()

# API limit 3/sec 3k per month
IMAGE_URL="https://api.cognitive.microsoft.com/bing/v7.0/images/search"
IMAGE_HEADER = {
'Ocp-Apim-Subscription-Key': '5781912b472246c0aa8b6bc898b1fbcb'
}

POSTAL_URL = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/postal/"

FACILITIES_IN_ZIP_URL = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/listings/"

FACILITY_DATA_URL = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/details/"

RECYCLABLE_IDS =[93, 429, 455,62, 61, 64, 245,83, 483,615,85, 488,645,529,332,
                   489,490,228,491,495,553,377,362,497,492,573]
FACILITY_TYPES = {
    0: 'Commercial',
    1: 'Government',
    3: 'Household Hazardous Waste',
    13: 'Non-Profit',
    28: 'National Retail',
    29: 'Regional Retail',
    30: 'Unstaffed Bin',
    31: 'Christmas Tree Events',
    32: 'ABOP'
}
seenkeys=[]
already_calculated=[]

def scrape_for_lat(id):
    headers = {'type':'location','ids': id}
    r = requests.get(FACILITY_DATA_URL, params=headers)
    if r.status_code == 200:
        json = r.json()
        result = json['result']
        if not id in result:
            return 0
        data = result[id]

        output = {
            'lat' : data['latitude'],
            'lng' : data['longitude'],
            'state' : data['province'],
            'name' : data['description']
        }
        seenkeys.append(id)
        time.sleep(1)
        facility = TempFacility(
            id=id,
            lat=output['lat'],
            lng=output['lng'],
            state=output['state']
        )
        #print(output['lat'], output['lng'])

        db.session.add(facility)
        db.session.commit()
        return 1
    return 0

if __name__ == '__main__':
    # instance = db.session.query(Facility, Facility.id).first()
    # print(instance.id)
    # scrape_for_lat(instance.id)
    already_done = [(thing.id) for thing in db.session.query(TempFacility, TempFacility.id)]
    for instance in db.session.query(Facility, Facility.id):
        if not instance.id in already_done:
            print(instance.id)
            scrape_for_lat(instance.id)
