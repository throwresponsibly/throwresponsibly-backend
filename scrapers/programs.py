import requests
from pprint import pprint
import flask
import flask_sqlalchemy
# import flask_restless
from sqlalchemy.dialects.postgresql import JSON
import urllib
import time

app = flask.Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:recycle123@throwresponsibly.me:5432/postgres'
db = flask_sqlalchemy.SQLAlchemy(app)

class Program(db.Model):
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    program_type=   db.Column(db.String(255))
    city=           db.Column(db.String(255))
    materials=      db.Column(JSON)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))

db.create_all()

# API limit 3/sec 3k per month
IMAGE_URL="https://api.cognitive.microsoft.com/bing/v7.0/images/search"
IMAGE_HEADER = {
'Ocp-Apim-Subscription-Key': '5781912b472246c0aa8b6bc898b1fbcb'
}

POSTAL_URL = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/postal/"

PROGRAMS_IN_ZIP_URL = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/listings/"

PROGRAM_DATA_URL = "https://recyclinglocator.earth911.com/recycling-locator/a0f1303a5d272ba2/details/"

RECYCLABLE_IDS =[93, 429, 455,62, 61, 64, 245,83, 483,615,85, 488,645,529,332, \
                    489,490,228,491,495,553,377,362,497,492,573]
PROGRAM_TYPES = {
    0: 'Resource Contact Information',
    1: 'Other',
    2: 'Government Packaging Curbside',
    3: 'Household Hazardous Waste',
    4: 'Mail-In',
    5: 'Commercial Curbside',
    6: 'Government Organics Curbside',
    7: 'Government Christmas Tree Curbside',
    8: 'Government Bulky Waste Curbside'
}
seenkeys=[]
already_calculated=[]

def screenvals(d):
    for key in d:
        if(d[key] == None or d[key] == ""):
            return False
    return True

def get_image_url(name):
    urlencoded_query = urllib.parse.quote(name)
    rurl = IMAGE_URL + "?" + "q=" + urlencoded_query

    image_search_result = requests.get(rurl, headers=IMAGE_HEADER)
    if image_search_result.status_code == 200:
        j = image_search_result.json()
        # j['value'][0]['thumbnailUrl'] # lower quality image
        val = j['value']
        if len(val) > 0:
            return j['value'][0]['contentUrl']
    return ""

def query_program_data_from_id(id):
    headers = {'type':'program','ids': id}
    r = requests.get(PROGRAM_DATA_URL, params=headers)
    if r.status_code == 200:
        json = r.json()
        result = json['result']
        data = result[id]
        materials = data['materials']
        materials_filtered = []

        dropoff='false'
        pickup = 'false'
        for mat in materials:
            materials_filtered.append({'desc': mat['description'], 'id': mat['material_id']})
            if 'dropoff' in mat:
                dropoff = mat['dropoff']
            if 'pickup' in mat:
                pickup = mat['pickup']

        program_type = data['program_type_id']
        output = {
            'name' : data['description'],
            'program_type' : PROGRAM_TYPES[int(data['program_type_id'])],
            'city': data['city'],
            'materials' : materials_filtered,
            'dropoff' : dropoff,
            'pickup' : pickup,
            'phone' : data['phone'],
            'address' : data['address'],
            'image_url' : get_image_url(data['description']),
            'url' : data['url']
        }
        time.sleep(1)
        seenkeys.append(id)
        if screenvals(output):
            program = Program(id=id,
                                name=output['name'],
                                program_type=output['program_type'],
                                city=output['city'],
                                materials=output['materials'],
                                dropoff=output['dropoff'],
                                pickup=output['pickup'],
                                phone=output['phone'],
                                address=output['address'],
                                image_url=output['image_url'],
                                url=output['url'])

            db.session.add(program)
            db.session.commit()
            return 1
        return 0

def query_location_for_resource_id(rec_id, lat, lng):
    headers = {'what':rec_id, 'lat':lat, 'lng':lng}
    r = requests.get(PROGRAMS_IN_ZIP_URL, params=headers)
    # l = list()
    if r.status_code == 200:
        json = r.json()
        if 'features' in json:
            features = json['features']
            i = 0
            for element in features:
                if(i > 2):
                    break

                geometry = element['geometry']
                properties = element['properties']
                if 'program_id' in properties:
                    id = properties['program_id']
                    if id in already_calculated:
                        i+=2
                        continue
                    if id not in seenkeys:
                        i += query_program_data_from_id(id)
    # return l

def scrape_programs_for_zip(zip:str):
    headers = {'postal_code' : zip}
    r = requests.get(POSTAL_URL, params=headers)
    # l = list()
    if r.status_code == 200:
        json = r.json()
        result = json['result']
        lat = "{0:.4f}".format(result['latitude'])
        lng = "{0:.4f}".format(result['longitude'])
        for rec_id in RECYCLABLE_IDS:
            query_location_for_resource_id(rec_id, lat,lng)
    # return l

if __name__ == '__main__':
    zips = ['35801', #Alabama
    '99501', #Alaska
    '85001', #Arizona
    '72201', #Arkansas
    '90001', #Califoria
    '80201', #Colrado
    '06101', #Connecticut
    '19901', #Delaware
    '32501', #Florida
    '30301', #Georgia
    '96801', #Hawaii
    '83254', #Idaho
    '60601', #Ilinois
    '46201', #Indiana
    '52801', #Iowa
    '67201', #Kansas
    '41701', #Kentucky
    '70112', #Lousiana
    '04032', #Maine
    '21201', #Maryland
    '02101', #Massachusetts
    '49036', #Michigan
    '55801', #Minnesota
    '39530',#Mississippi
    '63101',#Missouri
    '59044',#Montana
    '68901' ,#Nebraska
    '89501' ,#Nevada
    '03217',#New Hampshire
    '07039',#New Jersey
    '87500' ,#New Mexico
    '10001' ,#New York
    '27565',#North Carolina
    '58282',#North Dakota
    '44101' ,#Ohio
    '74101' ,#Oklahoma
    '97201' ,#Oregon
    '15201' ,#Pennsylvania
    '02840' ,#Rhode Island
    '29020',#South Carolina
    '57401' ,#South Dakota
    '37201' ,#Tennessee
    '78705' ,#Texas
    '84321' ,#Utah
    '05751',#Vermont
    '24517',#Virginia
    '98004',#Washington
    '25813',#West Virginia
    '53201',#Wisconsin
    '82941'#Wyoming
    ]
    for instance in db.session.query(Program, Program.id):
        already_calculated.append(instance.id)
    for zip in zips:
        scrape_programs_for_zip(zip)
    db.session.commit()
    # pprint(l)
