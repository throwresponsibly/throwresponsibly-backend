from flask import Flask, request, jsonify
import flask_sqlalchemy
import requests
from sqlalchemy.sql import text
from sqlalchemy.dialects.postgresql import JSON
import json
import math
from sqlalchemy.exc import IntegrityError

FAMILIES_URL = "https://api.earth911.com/earth911.getFamilies?api_key=10e91611cc8c3d96"
PROGRAM_DETAILS_URL = "http://api.earth911.com/earth911.getProgramDetails?api_key=10e91611cc8c3d96&program_id="
FACILITY_DETAILS_URL = ""

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:recycle123@throwresponsibly.me:5432/postgres'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = flask_sqlalchemy.SQLAlchemy(app)

class Facility(db.Model):
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    facility_type=  db.Column(db.String(255))
    city=           db.Column(db.String(255))
    materials=      db.Column(JSON)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'             :   self.id,
           'name'           :   self.name,
           'facility_type'  :   self.facility_type,
           'city'           :   self.city,
           'materials'      :   self.materials,
           'dropoff'        :   self.dropoff,
           'pickup'         :   self.pickup,
           'phone'          :   self.phone,
           'address'        :   self.address,
           'url'            :   self.url,
           'image_url'      :   self.image_url
       }

class Item(db.Model):
    id =            db.Column(db.Integer,primary_key=True)
    name =          db.Column(db.String(255))
    family_ids=      db.Column(JSON)
    long_desc=      db.Column(db.String(255))
    avg_price=      db.Column(db.String(255))
    listings=       db.Column(JSON)
    img_url=        db.Column(db.String(255))
    score=          db.Column(db.Float)
    definition=     db.Column(db.String(255))

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'             :   self.id,
           'name'           :   self.name,
           'family_ids'     :   self.family_ids,
           'long_desc'      :   self.long_desc,
           'avg_price'      :   self.avg_price,
           'listings'       :   self.listings,
           'img_url'        :   self.img_url,
           'score'          :   self.score,
           'definition'     :   self.definition
       }

class Program(db.Model):
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    program_type=   db.Column(db.String(255))
    city=           db.Column(db.String(255))
    materials=      db.Column(JSON)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'             :   self.id,
           'name'           :   self.name,
           'program_type'   :   self.program_type,
           'city'           :   self.city,
           'materials'      :   self.materials,
           'dropoff'        :   self.dropoff,
           'pickup'         :   self.pickup,
           'phone'          :   self.phone,
           'address'        :   self.address,
           'url'            :   self.url,
           'image_url'      :   self.image_url
       }


facility_to_program_association = db.Table('facility_to_program_association',
    db.Column('facility_id', db.String(255), db.ForeignKey('Facility_v2.id'), primary_key=True),
    db.Column('program_id', db.String(255), db.ForeignKey('Program_v2.id'), primary_key=True)
)

facility_to_program_map = {}

facility_to_item_association = db.Table('facility_to_item_association',
    db.Column('facility_id', db.String(255), db.ForeignKey('Facility_v2.id'), primary_key=True),
    db.Column('item_id', db.String(255), db.ForeignKey('Item_v2.id'), primary_key=True)
)

facility_to_item_map = {}

item_to_family_association = db.Table('item_to_family_association',
    db.Column('item_id', db.String(255), db.ForeignKey('Item_v2.id'), primary_key=True),
    db.Column('family_id', db.String(255), db.ForeignKey('Family_v2.id'), primary_key=True)
)

item_to_family_map = {}

program_to_item_association = db.Table('program_to_item_association',
    db.Column('program_id', db.String(255), db.ForeignKey('Program_v2.id'), primary_key=True),
    db.Column('item_id', db.String(255), db.ForeignKey('Item_v2.id'), primary_key=True)
)

program_to_item_map = {}

# program_to_facility_association = db.Table('program_to_facility_association',
#     db.Column('program_id', db.Integer, db.ForeignKey('Program_v2.id'), primary_key=True),
#     db.Column('facility_id', db.Integer, db.ForeignKey('Facility_v2.id'), primary_key=True)
# )

class Facility_v2(db.Model):
    __tablename__ = 'Facility_v2'
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    facility_type=  db.Column(db.String(255))
    city=           db.Column(db.String(255))
    programs=       db.relationship('Program_v2', secondary=facility_to_program_association)
    items=          db.relationship('Item_v2', secondary=facility_to_item_association)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))

class Family_Type_v2(db.Model):
    __tablename__ = 'Family_Type_v2'
    id          =   db.Column(db.String(255),primary_key=True)
    description =   db.Column(db.String(255))
    families    =   db.relationship('Family_v2', backref='Family_Type_v2', lazy=True)

class Family_v2(db.Model):
    __tablename__ = 'Family_v2'
    id          =   db.Column(db.String(255),primary_key=True)
    description =   db.Column(db.String(255))
    family_type =   db.Column(db.String(255), db.ForeignKey('Family_Type_v2.id'), nullable=False)

class Item_v2(db.Model):
    __tablename__ = 'Item_v2'
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    families=       db.relationship('Family_v2', secondary=item_to_family_association)
    long_desc=      db.Column(db.String(255))
    avg_price=      db.Column(db.String(255))
    listings=       db.Column(JSON)
    img_url=        db.Column(db.String(255))
    score=          db.Column(db.Float)
    definition=     db.Column(db.String(255))

class Program_v2(db.Model):
    __tablename__ = 'Program_v2'
    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    program_type=   db.Column(db.String(255))
    city=           db.Column(db.String(255))
    items=          db.relationship('Item_v2', secondary=program_to_item_association)
    facilities=     db.relationship('Facility_v2', secondary=facility_to_program_association)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))

family_type_list = [Family_Type_v2(id='1', description='Common'),
Family_Type_v2(id='2', description='Business'),
Family_Type_v2(id='3', description='Partner'),
Family_Type_v2(id='4', description='Legacy'),
Family_Type_v2(id='5', description='Microsite'),
Family_Type_v2(id='6', description='Directory')]

family_map = {}
item_v2_map = {}
program_v2_map = {}
facility_v2_map = {}

facilities = Facility.query.all()
items = Item.query.all()
programs = Program.query.all()

facility_item_map = {}
facility_map = {}
program_item_map = {}
program_map = {}
item_map = {}

facilities_appended_to_program = set()
items_appended_to_program = set()

programs_appended_to_facility = set()
items_appended_to_facility = set()

def augment_items():
    global item_v2_map
    global family_map
    print("Augmenting items")
    for i in items:
        my_item = Item_v2(
            id = str(i.id),
            name = i.name,
            long_desc= i.long_desc,
            avg_price= i.avg_price,
            listings=  i.listings,
            img_url=   i.img_url,
            score= i.score,
            definition=i.definition)
        for family_id in i.family_ids:
            if str(family_id) in family_map:
                my_item.families.append(family_map[str(family_id)])

        item_v2_map[i.id] = my_item

        data = i.serialize
        item_map[i.id] = data

        filtered_facilities = []
        filtered_facilities_names = []
        # get all facilities and programs and filter down on which contain this item in its materials list
        for f in facilities:
            for item in f.materials:
                if item['id'] == i.id:
                    if i.id in facility_item_map:
                        current_list = facility_item_map[i.id]
                        if f.serialize not in current_list:
                            current_list.append(f.serialize)
                    else:
                        facility_item_map[i.id] = [f.serialize]

                    if f.name not in filtered_facilities_names:
                        filtered_facilities.append(f.serialize)
                        filtered_facilities_names.append(f.name)
                        break
        data['facilities'] = filtered_facilities
        data['facilities_names'] = filtered_facilities_names

        filtered_programs = []
        filtered_programs_names = []
        for p in programs:
            for item in p.materials:
                if item['id'] == i.id:
                    if i.id in program_item_map:
                        current_list = program_item_map[i.id]
                        if p.serialize not in current_list:
                            current_list.append(p.serialize)
                    else:
                        program_item_map[i.id] = [p.serialize]

                    if p.name not in filtered_programs_names:
                        filtered_programs.append(p.serialize)
                        filtered_programs_names.append(p.name)
                        break

    print("Done augmenting items")

def augment_programs():
    global program_v2_map
    global item_v2_map
    global facility_v2_map
    print("Augmenting programs")
    for p in programs:
        my_program = None
        if p.id in program_v2_map:
            my_program = program_v2_map[p.id]
        else:
            my_program = Program_v2(
                id =            p.id,
                name =          p.name,
                program_type=   p.program_type,
                city=           p.city,
                dropoff =       p.dropoff,
                pickup =        p.pickup,
                phone =         p.phone,
                address=        p.address,
                url =           p.url,
                image_url=      p.image_url
                )
            program_v2_map[p.id] = my_program
        data = p.serialize

        facilities_shared_items = []
        facilities_shared_items_names = []
        program_items = []
        program_items_names = []

        for material in p.materials:
            if material['id'] in facility_item_map:
                # add facilities which have items in common
                common_facilities = facility_item_map[material['id']]
                for c in common_facilities:
                    if c['name'] not in facilities_shared_items_names:
                        facilities_shared_items_names.append(c['name'])
                        facilities_shared_items.append(c)
                        # construct a new facility_v2 from c or check if it exists in map
                        # add this facility to the program facilities
                        if c['id'] in facility_v2_map:
                            if c['id'] not in facilities_appended_to_program:
                                facilities_appended_to_program.add(c['id'])
                                my_program.facilities.append(facility_v2_map[c['id']])
                        else:
                            # contstruct a new facility_v2
                            my_facility = Facility_v2(
                                id =            c['id'],
                                name =          c['name'],
                                facility_type=  c['facility_type'],
                                city=           c['city'],
                                dropoff =       c['dropoff'],
                                pickup =        c['pickup'],
                                phone =         c['phone'],
                                address=        c['address'],
                                url =           c['url'],
                                image_url=      c['image_url']
                            )
                            if c['id'] not in facilities_appended_to_program:
                                facilities_appended_to_program.add(c['id'])
                                my_program.facilities.append(my_facility)
                            facility_v2_map[c['id']] = my_facility

            if material['id'] in item_map:
                # keep track of total item list for this program
                common_item = item_map[material['id']]
                # construct a new item_v2 from common_item or check if it exists in map
                # add this item_v2 to the program facilities
                if common_item['name'] not in program_items_names:
                    if common_item['id'] in item_v2_map:
                        if common_item['id'] not in items_appended_to_program:
                            items_appended_to_program.add(common_item['id'])
                            my_program.items.append(item_v2_map[common_item['id']])
                    else:
                        # construct a new item_v2
                        my_item = Item_v2(
                            id =            str(common_item['id']),
                            name =          common_item['name'],
                            families=       common_item['families'],
                            long_desc=      common_item['long_desc'],
                            avg_price=      common_item['avg_price'],
                            listings=       common_item['listings'],
                            img_url=        common_item['img_url'],
                            score=          common_item['score'],
                            definition=     common_item['definition']
                        )
                        if common_item['id'] not in items_appended_to_program:
                            items_appended_to_program.add(common_item['id'])
                            my_program.items.append(my_item)
                        item_v2_map[common_item['id']] = my_item
                    program_items_names.append(common_item['name'])
                    program_items.append(common_item)

    print("Done augmenting programs")

def augment_facilities():
    global program_v2_map
    global item_v2_map
    global facility_v2_map
    print("Augmenting facilities")

    for f in facilities:
        my_facility = None
        if f.id in facility_v2_map:
            my_facility = facility_v2_map[f.id]
        else:
            my_facility = Facility_v2(
                id =            f.id,
                name =          f.name,
                facility_type=  f.facility_type,
                city=           f.city,
                dropoff =       f.dropoff,
                pickup =        f.pickup,
                phone =         f.phone,
                address=        f.address,
                url =           f.url,
                image_url=      f.image_url
            )

            facility_v2_map[f.id] = my_facility

        data = f.serialize

        facility_map[f.id] = data

        programs_shared_items = []
        programs_shared_items_names = []
        facility_items = []
        facility_items_names = []

        for material in f.materials:
            if material['id'] in program_item_map:
                # add facilities which have items in common
                common_programs = program_item_map[material['id']]
                for c in common_programs:
                    if c['name'] not in programs_shared_items_names:
                        programs_shared_items_names.append(c['name'])
                        programs_shared_items.append(c)
                        # construct a new program_v2 from c or check if it exists in map
                        # add this program_v2 to the facility
                        if c['id'] in program_v2_map:
                            if c['id'] not in programs_appended_to_facility:
                                programs_appended_to_facility.add(c['id'])
                                my_facility.programs.append(program_v2_map[c['id']])
                        else:
                            # contstruct a new facility_v2
                            my_program = Program_v2(
                                id =            c['id'],
                                name =          c['name'],
                                program_type=   c['program_type'],
                                city=           c['city'],
                                dropoff =       c['dropoff'],
                                pickup =        c['pickup'],
                                phone =         c['phone'],
                                address=        c['address'],
                                url =           c['url'],
                                image_url=      c['image_url']
                                )
                            if c['id'] not in programs_appended_to_facility:
                                programs_appended_to_facility.add(c['id'])
                                my_facility.programs.append(my_program)
                            program_v2_map[c['id']] = my_program

            if material['id'] in item_map:
                # keep track of total item list for this program
                common_item = item_map[material['id']]
                if common_item['name'] not in facility_items_names:
                    if common_item['id'] in item_v2_map:
                        if common_item['id'] not in items_appended_to_facility:
                            items_appended_to_facility.add(common_item['id'])
                            my_facility.items.append(item_v2_map[common_item['id']])
                    else:
                        # construct a new item_v2
                        my_item = Item_v2(
                            id =            str(common_item['id']),
                            name =          common_item['name'],
                            families=       common_item['families'],
                            long_desc=      common_item['long_desc'],
                            avg_price=      common_item['avg_price'],
                            listings=       common_item['listings'],
                            img_url=        common_item['img_url'],
                            score=          common_item['score'],
                            definition=     common_item['definition']
                        )
                        if common_item['id'] not in items_appended_to_facility:
                            items_appended_to_facility.add(common_item['id'])
                            my_facility.items.append(my_item)
                        item_v2_map[common_item['id']] = my_item
                    facility_items_names.append(common_item['name'])
                    facility_items.append(common_item)

    print("Done augmenting facilities")

def getFamiliesAndType():
    global family_type_list
    global family_map
    r = requests.get(FAMILIES_URL)
    if(r.status_code == 200):
        result = r.json()['result']
        for family in result:
            description = family['description']
            family_id = str(family['family_id'])
            family_type_id = str(family['family_type_id'])
            family = Family_v2(id=family_id, description=description, family_type=family_type_id)
            family_type_list[int(family_type_id)-1].families.append(family)
            if family_id not in family_map:
                family_map[family_id] = family

def flush_to_database():
    length = len(family_map)
    count = 0
    for key in family_map:
        count += 1
        print(str(count)+"/"+str(length))
        try:
            db.session.merge(family_map[key])
            db.session.commit()
        except IntegrityError:
            print("Duplicate Value entered 0")
            db.session.rollback()

    length = len(item_v2_map)
    count = 0
    for key in item_v2_map:
        count += 1
        print(str(count)+"/"+str(length))
        try:
            db.session.merge(item_v2_map[key])
            db.session.commit()
        except IntegrityError:
            print("Duplicate Value entered 1")
            db.session.rollback()

    length = len(facility_v2_map)
    count = 0
    for key in facility_v2_map:
        count += 1
        print(str(count)+"/"+str(length))
        try:
            db.session.add(facility_v2_map[key])
            db.session.commit()
        except IntegrityError:
            print("Duplicate Value entered 2")
            db.session.rollback()

    length = len(program_v2_map)
    count = 0
    for key in program_v2_map:
        count += 1
        print(str(count)+"/"+str(length))
        try:
            db.session.merge(program_v2_map[key])
            db.session.commit()
        except IntegrityError:
            print("Duplicate Value entered 3")
            db.session.rollback()

    #db.session.commit()

if __name__ == '__main__':
    db.create_all()
    getFamiliesAndType()

    # compute relations between models
    augment_items()
    augment_programs()
    augment_facilities()

    flush_to_database()
