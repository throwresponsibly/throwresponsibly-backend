import unittest
import app
from requests import request
from test_responses import *


production_site_url = 'http://throwresponsibly.me/'
production_api_url = 'http://throwresponsibly.me:55/'


class TestStringMethods(unittest.TestCase):
    def test1(self):
        print('\n -- TESTING PRODUCTION SITE STATUS --')
        response = request('GET', production_site_url)
        self.assertEqual(response.ok, True)
        print('\nTEST 1: Production site is live.')

    def test2(self):
        response = request('GET', production_api_url)
        self.assertEqual(response.ok, True)
        print('\nTEST 2: Production API is live.')

    def test3(self):
        response = request('GET', production_api_url + 'items')
        self.assertEqual(response.ok, True)
        print('\nTEST 3.1: API "/items" endpoint is live.')

    def test4(self):
        response = request('GET', production_api_url + 'items/456')
        self.assertEqual(response.ok, True)
        print('\nTEST 4: API "/items/<id>" endpoint is live.')

    def test5(self):
        response = request('GET', production_api_url + 'facilities')
        self.assertEqual(response.ok, True)
        print('\nTEST 5: API "/facilities" endpoint is live.')

    def test6(self):
        response = request('GET', production_api_url + 'facilities/Q1RQNVJfW1xGUA')
        self.assertEqual(response.ok, True)
        print('\nTEST 6: API "/facilities/<id>" endpoint is live.')

    def test7(self):
        response = request('GET', production_api_url + 'programs')
        self.assertEqual(response.ok, True)
        print('\nTEST 7: API "/programs" endpoint is live.')

    def test8(self):
        response = request('GET', production_api_url + 'programs/Q1RQNVJZXV5EXQ')
        self.assertEqual(response.ok, True)
        print('\nTEST 8: API "/programs/<id>" endpoint is live.')

    def test_method1(self):
        print('\n-- TESTING ENDPOINT METHODS --')
        with app.app.test_request_context('/items', data={'page': '1',
                                                          'size': '5'}):
            self.assertEqual(app.all_items().json, ALL_ITEMS_JSON)
        print('\nTEST 9: Method "all_items" returned expected result.')

    def test_method2(self):
        with app.app.test_request_context('/facilites', data={'page': '3',
                                                              'size': '4'}):
            self.assertEqual(app.all_facilities().json, ALL_FACILITIES_JSON)
        print('\nTEST 10: Method "all_facilities" returned expected result.')

    def test_method3(self):
        with app.app.test_request_context('/programs', data={'page': '0',
                                                             'size': '7'}):
            self.assertEqual(app.all_programs().json, ALL_PROGRAMS_JSON)
        print('\nTEST 11: Method "all_programs" returned expected result.')


if __name__ == '__main__':
    # build relations for api.v1
    app.augment_items()
    app.augment_programs()
    app.augment_facilities()

    print('\n\n******* BACKEND UNIT TESTS *******\n')
    unittest.main()
