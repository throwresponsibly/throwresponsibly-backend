from flask import Flask, request, jsonify
import flask_sqlalchemy
from sqlalchemy.sql import text
from sqlalchemy.dialects.postgresql import JSON
from flask_cors import CORS
from random import uniform
from urllib.parse import urljoin
import json
import math

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:recycle123@throwresponsibly.me:5432/postgres'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = flask_sqlalchemy.SQLAlchemy(app)
CORS(app)

class Facility(db.Model):
    __tablename__ = 'new_facility'

    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    facility_type=  db.Column(db.String(255))
    city=           db.Column(db.String(255))
    materials=      db.Column(JSON)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))
    lat =           db.Column(db.Float())
    lng =           db.Column(db.Float())

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'             :   self.id,
           'name'           :   self.name,
           'facility_type'  :   self.facility_type,
           'city'           :   self.city,
           'materials'      :   self.materials,
           'dropoff'        :   self.dropoff,
           'pickup'         :   self.pickup,
           'phone'          :   self.phone,
           'address'        :   self.address,
           'url'            :   self.url,
           'image_url'      :   self.image_url,
           'lat'            :   self.lat,
           'lng'            :   self.lng
       }

class Item(db.Model):
    id =            db.Column(db.Integer,primary_key=True)
    name =          db.Column(db.String(255))
    family_ids=      db.Column(JSON)
    long_desc=      db.Column(db.String(255))
    avg_price=      db.Column(db.String(255))
    listings=       db.Column(JSON)
    img_url=        db.Column(db.String(255))
    score=          db.Column(db.Float)
    definition=     db.Column(db.String(255))

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'             :   self.id,
           'name'           :   self.name,
           'family_ids'     :   self.family_ids,
           'long_desc'      :   self.long_desc,
           'avg_price'      :   self.avg_price,
           'listings'       :   self.listings,
           'img_url'        :   self.img_url,
           'score'          :   self.score,
           'definition'     :   self.definition
       }

class Program(db.Model):
    __tablename__ = 'new_program'

    id =            db.Column(db.String(255),primary_key=True)
    name =          db.Column(db.String(255))
    program_type=   db.Column(db.String(255))
    city=           db.Column(db.String(255))
    materials=      db.Column(JSON)
    dropoff =       db.Column(db.Boolean())
    pickup =        db.Column(db.Boolean())
    phone =         db.Column(db.String(255))
    address=        db.Column(db.String(255))
    url =           db.Column(db.String(255))
    image_url=      db.Column(db.String(255))
    lat =           db.Column(db.Float())
    lng =           db.Column(db.Float())
    state =         db.Column(db.String(255))

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'             :   self.id,
           'name'           :   self.name,
           'program_type'   :   self.program_type,
           'city'           :   self.city,
           'materials'      :   self.materials,
           'dropoff'        :   self.dropoff,
           'pickup'         :   self.pickup,
           'phone'          :   self.phone,
           'address'        :   self.address,
           'url'            :   self.url,
           'image_url'      :   self.image_url,
           'lat'            :   self.lat,
           'lng'            :   self.lng,
           'state'          :   self.state
       }

facilities = Facility.query.all()
facilities_augmented = None
items = Item.query.all()
items_augmented = None
programs = Program.query.all()
programs_augmented = None

facility_item_map = {}
facility_map = {}
program_item_map = {}
program_map = {}
item_map = {}

# facilities_by_item_sql = """
# SELECT *
# FROM facility f, json_array_elements(f.materials) obj
# WHERE  obj->>'id' = '{}';
# """

def collect_headers():
    expand_facilities = None
    expand_programs = None
    expand_items = None
    if 'Expand-Items' in request.headers:
        expand_items = request.headers['Expand-Items']
    if 'Expand-Facilities' in request.headers:
        expand_facilities = request.headers['Expand-Facilities']
    if 'Expand-Programs' in request.headers:
        expand_programs = request.headers['Expand-Programs']
    return [expand_facilities, expand_programs, expand_items]

@app.route('/', methods=['GET'])
def root():
    base_url = 'http://api.throwresponsibly.me'
    routes = {
        'facilities_url': urljoin(base_url, 'facilities'),
        'facility_url': urljoin(base_url, 'facilities/{facility_id}'),
        'items_url': urljoin(base_url, 'items'),
        'item_url': urljoin(base_url, 'items/{item_id}'),
        'programs_url': urljoin(base_url, 'programs'),
        'program_url': urljoin(base_url, 'programs/{program_id}')
    }
    return jsonify(routes)

@app.route('/items')
def all_items():
    expand_facilities, expand_programs, expand_items = collect_headers()

    page = request.args.get('page')
    size = request.args.get('size')

    sort = request.args.get('sort')
    score_filter = request.args.get('score_filter')
    family_filter = request.args.get('family_filter')
    search = request.args.get('search')
    sort_order = request.args.get('sort_order')
    result = compute_all_items(expand_facilities, expand_programs, page, size, sort, score_filter, family_filter, search, sort_order)
    return jsonify(result)

@app.route('/items/<int:item_id>')
def get_item(item_id):
    expand_facilities, expand_programs, expand_items = collect_headers()
    data = item_map[item_id].copy()

    if expand_facilities == 'name':
        data.pop('facilities', None)
    elif expand_facilities == 'short':
        data.pop('facilities', None)
        data.pop('facilities_names', None)
        data['facilities_url'] = '/items/'+str(data['id'])+'/facilities'

    if  expand_programs == 'name':
        data.pop('programs', None)
    elif expand_programs == 'short':
        data.pop('programs', None)
        data.pop('programs_names', None)
        data['programs_url'] = '/items/'+str(data['id'])+'/programs'

    return jsonify(data)

def sanitize_pagination_bounds(page, size, iteration_list):
    if page == None:
        page = 1
    if size == None:
        size = 5
    page = int(page)
    size = int(size)
    size = max(1, size)

    max_page = math.ceil(len(iteration_list)/size)
    page = min(max_page+1, page)

    start_index = (page-1)*size
    end_index = min(page*size, len(iteration_list))

    return [page, size, start_index, end_index, max_page]

def is_number(n):
    try:
        float(n)   # Type-casting the string to `float`.
                   # If string is not a valid `float`,
                   # it'll raise `ValueError` exception
    except ValueError:
        return False
    return True

def compute_all_items(expand_facilities, expand_programs, page, size, sort, score_filter, family_filter, search, sort_order):
    if expand_facilities == None:
        expand_facilities = 'name'
    if expand_programs == None:
        expand_programs = 'name'

    output = {}
    result = []

    sorted_items = None
    if sort == 'name':
        if sort_order == "DESC":
            sorted_items = sorted(items_augmented, key=lambda x: x['name'], reverse=True)
        else:
            sorted_items = sorted(items_augmented, key=lambda x: x['name'])
    elif sort == 'price':
        if sort_order == "DESC":
            sorted_items = sorted(items_augmented, key=lambda x: float(x['avg_price']), reverse=True)
        else:
            sorted_items = sorted(items_augmented, key=lambda x: float(x['avg_price']))
    elif sort == 'id':
        if sort_order == "DESC":
            sorted_items = sorted(items_augmented, key=lambda x: x['id'], reverse=True)
        else:
            sorted_items = sorted(items_augmented, key=lambda x: x['id'])
    else:
        sorted_items = items_augmented

    sorted_items_filtered = []
    for original_data in sorted_items:
        data = original_data.copy()
        scoreText = ""
        if is_number(data['score']):
            if data['score'] < 100:
                scoreText = "Really Hard"
            elif data['score'] < 300:
                scoreText = "Hard"
            elif data['score'] < 500:
                scoreText = "Medium"
            elif data['score'] < 1000:
                scoreText = "Easy"
            elif data['score'] >= 1000:
                scoreText = "Really Easy"

            data['score'] = scoreText
            original_data['score'] = scoreText

        scoreText = data['score']
        data['avg_price'] = str(round(float(data['avg_price']), 2))

        filtered_out = False
        if score_filter != None and scoreText != score_filter:
            filtered_out = True
        if family_filter != None:
            families = data['family_ids']
            family_filter = int(family_filter)
            if family_filter not in families:
                filtered_out = True
        if (not filtered_out) and (search != None):
            terms = search.split(" ")
            to_be_searched_list = ['name', 'avg_price', 'id', 'score']
            data['search_hit'] = []
            for original_term in terms:
                term = original_term.lower()
                for attribute_searched in to_be_searched_list:
                    if isinstance(data[attribute_searched], int):
                        if term.isdigit():
                            attribute_searched_int = data[attribute_searched]
                            term = int(term)
                            if term == attribute_searched_int and original_term not in data['search_hit'] and term not in data['search_hit']:
                                data[attribute_searched] = "<b>"+str(term)+"</b>"
                                data['search_hit'].append(original_term)
                    elif isinstance(data[attribute_searched], float):
                        try:
                            attribute_searched_float = data[attribute_searched]
                            term = float(term)
                            if term == attribute_searched_float and original_term not in data['search_hit'] and term not in data['search_hit']:
                                data[attribute_searched] = "<b>"+str(term)+"</b>"
                                data['search_hit'].append(original_term)
                        except ValueError:
                            pass
                    else:
                        attribute_searched_lowercase = data[attribute_searched].lower()
                        if term in attribute_searched_lowercase and original_term not in data['search_hit'] and term not in data['search_hit']:
                            start_search_index = attribute_searched_lowercase.index(term)
                            end_search_index = start_search_index+ len(term)
                            data[attribute_searched] = data[attribute_searched][0:start_search_index]+"<b>"+data[attribute_searched][start_search_index:end_search_index] + "</b>" + data[attribute_searched][end_search_index:]
                            data['search_hit'].append(original_term)
                term = str(term)
                # two special cases - family_ids (list) and score (number has to be converted to string)
                # family_ids
                data['family_ids'] = data['family_ids'].copy()
                for family_id in data['family_ids']:
                    attribute_searched_lowercase = str(family_id)
                    family_id_index = data['family_ids'].index(family_id)
                    if term == attribute_searched_lowercase and original_term not in data['search_hit'] and term not in data['search_hit']:
                        data['family_ids'][family_id_index] = "<b>"+str(family_id)+"</b>"
                        data['search_hit'].append(original_term)
            if len(data['search_hit']) == 0:
                filtered_out = True
        if not filtered_out:
            sorted_items_filtered.append(data)

    page, size, start_index, end_index, max_page = sanitize_pagination_bounds(page, size, sorted_items_filtered)

    for original_data in sorted_items_filtered[start_index:end_index]:
        data = original_data.copy()
        if expand_facilities == 'name':
            data.pop('facilities', None)
        elif expand_facilities == 'short':
            data.pop('facilities', None)
            data.pop('facilities_names', None)
            data['facilities_url'] = '/items/'+str(data['id'])+'/facilities'

        if  expand_programs == 'name':
            data.pop('programs', None)
        elif expand_programs == 'short':
            data.pop('programs', None)
            data.pop('programs_names', None)
            data['programs_url'] = '/items/'+str(data['id'])+'/programs'
        result.append(data)
    output['result'] = result
    output['page'] = page
    output['size'] = size
    output['max_page'] = max_page
    output['next'] = "/items?page="+str(page+1)+"&size="+str(size)
    return output


@app.route('/programs')
def all_programs():
    expand_facilities, expand_programs, expand_items = collect_headers()

    page = request.args.get('page')
    size = request.args.get('size')
    sort = request.args.get('sort')
    city_filter = request.args.get('city_filter')
    dropoff_filter = request.args.get('dropoff_filter')
    pickup_filter = request.args.get('pickup_filter')
    search = request.args.get('search')
    sort_order = request.args.get('sort_order')

    result = compute_all_programs(expand_facilities, expand_items, page, size, sort, city_filter, dropoff_filter, pickup_filter, search, sort_order)
    return jsonify(result)

@app.route('/programs/<program_id>')
def get_program(program_id):
    expand_facilities, expand_programs, expand_items = collect_headers()
    data = program_map[program_id].copy()

    if expand_facilities == 'name':
        data.pop('facilities', None)
    elif expand_facilities == 'short':
        data.pop('facilities', None)
        data.pop('facilities_names', None)
        data['facilities_url'] = '/items/'+str(data['id'])+'/facilities'

    if  expand_items == 'name':
        data.pop('items', None)
    elif expand_items == 'short':
        data.pop('items', None)
        data.pop('items_names', None)
        data['items_url'] = '/programs/'+str(data['id'])+'/items'

    return jsonify(data)

def compute_all_programs(expand_facilities, expand_items, page, size, sort, city_filter, dropoff_filter, pickup_filter, search, sort_order):
    if expand_facilities == None:
        expand_facilities = 'name'
    if expand_items == None:
        expand_items = 'name'

    output = {}
    result = []

    sorted_programs = None
    if sort == 'name':
        if sort_order == "DESC":
            sorted_programs = sorted(programs_augmented, key=lambda x: x['name'], reverse=True)
        else:
            sorted_programs = sorted(programs_augmented, key=lambda x: x['name'])
    elif sort == 'city':
        if sort_order == "DESC":
            sorted_programs = sorted(programs_augmented, key=lambda x: x['city'], reverse=True)
        else:
            sorted_programs = sorted(programs_augmented, key=lambda x: x['city'])
    else:
        sorted_programs = programs_augmented

    sorted_programs_filtered = []
    for original_data in sorted_programs:
        data = original_data.copy()
        filtered_out = False
        dropff = str(data['dropoff'])
        pickup = str(data['pickup'])
        if city_filter != None:
            if data['city'] != city_filter:
                filtered_out = True
        if dropoff_filter != None:
            if dropff != dropoff_filter:
                filtered_out = True
        if pickup_filter != None:
            if pickup != pickup_filter:
                filtered_out = True
        if (not filtered_out) and (search != None):
            terms = search.split(" ")
            to_be_searched_list = ['address', 'city', 'name', 'phone', 'program_type']
            data['search_hit'] = []
            for original_term in terms:
                term = original_term.lower()
                for attribute_searched in to_be_searched_list:
                    attribute_searched_lowercase = data[attribute_searched].lower()
                    if term in attribute_searched_lowercase and original_term not in data['search_hit'] and term not in data['search_hit']:
                        start_search_index = attribute_searched_lowercase.index(term)
                        end_search_index = start_search_index+ len(term)
                        data[attribute_searched] = data[attribute_searched][0:start_search_index]+"<b>"+data[attribute_searched][start_search_index:end_search_index] + "</b>" + data[attribute_searched][end_search_index:]
                        data['search_hit'].append(original_term)\
                # special case of 'dropoff' and 'pickup'
                if (data['dropoff'] == True) and (term == 'dropoff') and (original_term not in data['search_hit']) and (term not in data['search_hit']):
                    data['search_hit'].append(original_term)
                    data['dropoff'] = "true"
                if (data['pickup'] == True) and (term == 'pickup') and (original_term not in data['search_hit']) and (term not in data['search_hit']):
                    data['search_hit'].append(original_term)
                    data['pickup'] = "true"
            if len(data['search_hit']) == 0:
                filtered_out = True
        if not filtered_out:
            sorted_programs_filtered.append(data)

    page, size, start_index, end_index, max_page = sanitize_pagination_bounds(page, size, sorted_programs_filtered)

    for original_data in sorted_programs_filtered[start_index:end_index]:
        data = original_data.copy()
        if expand_facilities == 'name':
            data.pop('facilities', None)
        elif expand_facilities == 'short':
            data.pop('facilities', None)
            data.pop('facilities_names', None)
            data['facilities_url'] = '/programs/'+str(data['id'])+'/facilities'

        if  expand_items == 'name':
            data.pop('items', None)
        elif expand_items == 'short':
            data.pop('items', None)
            data.pop('items_names', None)
            data['items_url'] = '/programs/'+str(data['id'])+'/items'
        result.append(data)
    output['result'] = result
    output['page'] = page
    output['size'] = size
    output['max_page'] = max_page
    output['next'] = "/programs?page="+str(page+1)+"&size="+str(size)
    return output

@app.route('/facilities')
def all_facilities():
    expand_facilities, expand_programs, expand_items = collect_headers()

    page = request.args.get('page')
    size = request.args.get('size')
    sort = request.args.get('sort')
    city_filter = request.args.get('city_filter')
    dropoff_filter = request.args.get('dropoff_filter')
    pickup_filter = request.args.get('pickup_filter')
    search = request.args.get('search')
    sort_order = request.args.get('sort_order')

    result = compute_all_facilities(expand_programs, expand_items, page, size, sort, city_filter, dropoff_filter, pickup_filter, search, sort_order)
    return jsonify(result)

@app.route('/facilities/<facility_id>')
def get_facility(facility_id):
    expand_facilities, expand_programs, expand_items = collect_headers()
    data = facility_map[facility_id].copy()

    if expand_programs == 'name':
        data.pop('programs', None)
    elif expand_programs == 'short':
        data.pop('programs', None)
        data.pop('programs_names', None)
        data['programs_url'] = '/facilities/'+str(data['id'])+'/programs'

    if  expand_items == 'name':
        data.pop('items', None)
    elif expand_items == 'short':
        data.pop('items', None)
        data.pop('items_names', None)
        data['items_url'] = '/programs/'+str(data['id'])+'/items'

    return jsonify(data)

def compute_all_facilities(expand_programs, expand_items, page, size, sort, city_filter, dropoff_filter, pickup_filter, search, sort_order):
    if expand_programs == None:
        expand_programs = 'name'
    if expand_items == None:
        expand_items = 'name'

    output = {}
    result = []

    sorted_facilities = None
    if sort == 'name':
        if sort_order == "DESC":
            sorted_facilities = sorted(facilities_augmented, key=lambda x: x['name'], reverse=True)
        else:
            sorted_facilities = sorted(facilities_augmented, key=lambda x: x['name'])
    elif sort == 'city':
        if sort_order == "DESC":
            sorted_facilities = sorted(facilities_augmented, key=lambda x: x['city'], reverse=True)
        else:
            sorted_facilities = sorted(facilities_augmented, key=lambda x: x['city'])
    else:
        sorted_facilities = facilities_augmented

    sorted_facilities_filtered = []
    for original_data in sorted_facilities:
        data = original_data.copy()
        filtered_out = False
        dropff = str(data['dropoff'])
        pickup = str(data['pickup'])
        if city_filter != None:
            if data['city'] != city_filter:
                filtered_out = True
        if dropoff_filter != None:
            if dropff != dropoff_filter:
                filtered_out = True
        if pickup_filter != None:
            if pickup != pickup_filter:
                filtered_out = True
        if (not filtered_out) and (search != None):
            terms = search.split(" ")
            to_be_searched_list = ['address', 'city', 'name', 'phone', 'facility_type']
            data['search_hit'] = []
            for original_term in terms:
                term = original_term.lower()
                for attribute_searched in to_be_searched_list:
                    attribute_searched_lowercase = data[attribute_searched].lower()
                    if term in attribute_searched_lowercase and original_term not in data['search_hit'] and term not in data['search_hit']:
                        start_search_index = attribute_searched_lowercase.index(term)
                        end_search_index = start_search_index+ len(term)
                        data[attribute_searched] = data[attribute_searched][0:start_search_index]+"<b>"+data[attribute_searched][start_search_index:end_search_index] + "</b>" + data[attribute_searched][end_search_index:]
                        data['search_hit'].append(original_term)
                # special case of 'dropoff' and 'pickup'
                if (data['dropoff'] == True) and (term == 'dropoff') and (original_term not in data['search_hit']) and (term not in data['search_hit']):
                    data['search_hit'].append(original_term)
                    data['dropoff'] = "true"
                if (data['pickup'] == True) and (term == 'pickup') and (original_term not in data['search_hit']) and (term not in data['search_hit']):
                    data['search_hit'].append(original_term)
                    data['pickup'] = "true"

            if len(data['search_hit']) == 0:
                filtered_out = True
        if not filtered_out:
            sorted_facilities_filtered.append(data)

    page, size, start_index, end_index, max_page = sanitize_pagination_bounds(page, size, sorted_facilities_filtered)

    for original_data in sorted_facilities_filtered[start_index:end_index]:
        data = original_data.copy()
        if expand_programs == 'name':
            data.pop('programs', None)
        elif expand_programs == 'short':
            data.pop('programs', None)
            data.pop('programs_names', None)
            data['programs_url'] = '/facilities/'+str(data['id'])+'/programs'

        if  expand_items == 'name':
            data.pop('items', None)
        elif expand_items == 'short':
            data.pop('items', None)
            data.pop('items_names', None)
            data['items_url'] = '/programs/'+str(data['id'])+'/items'
        result.append(data)
    output['result'] = result
    output['page'] = page
    output['size'] = size
    output['max_page'] = max_page
    output['next'] = "/facilities?page="+str(page+1)+"&size="+str(size)
    return output

def augment_items():
    global items_augmented
    print("Augmenting items")
    result = []
    for i in items:
        data = i.serialize

        item_map[i.id] = data

        filtered_facilities = []
        filtered_facilities_names = []
        # get all facilities and programs and filter down on which contain this item in its materials list
        for f in facilities:
            for item in f.materials:
                if item['id'] == i.id:
                    if i.id in facility_item_map:
                        current_list = facility_item_map[i.id]
                        if f.serialize not in current_list:
                            current_list.append(f.serialize)
                    else:
                        facility_item_map[i.id] = [f.serialize]

                    if f.name not in filtered_facilities_names:
                        filtered_facilities.append(f.serialize)
                        filtered_facilities_names.append(f.name)
                        break
        data['facilities'] = filtered_facilities
        data['facilities_names'] = filtered_facilities_names

        filtered_programs = []
        filtered_programs_names = []
        for p in programs:
            for item in p.materials:
                if item['id'] == i.id:
                    if i.id in program_item_map:
                        current_list = program_item_map[i.id]
                        if p.serialize not in current_list:
                            current_list.append(p.serialize)
                    else:
                        program_item_map[i.id] = [p.serialize]

                    if p.name not in filtered_programs_names:
                        filtered_programs.append(p.serialize)
                        filtered_programs_names.append(p.name)
                        break
        data['programs'] = filtered_programs
        data['programs_names'] = filtered_programs_names
        result.append(data)
    items_augmented = result
    print("Done augmenting items")

def augment_programs():
    global programs_augmented
    print("Augmenting programs")
    result = []
    for p in programs:
        data = p.serialize

        program_map[p.id] = data

        facilities_shared_items = []
        facilities_shared_items_names = []
        program_items = []
        program_items_names = []

        for material in p.materials:
            if material['id'] in facility_item_map:
                # add facilities which have items in common
                common_facilities = facility_item_map[material['id']]
                for c in common_facilities:
                    if c['name'] not in facilities_shared_items_names:
                        facilities_shared_items_names.append(c['name'])
                        facilities_shared_items.append(c)

            if material['id'] in item_map:
                # keep track of total item list for this program
                common_item = item_map[material['id']]
                if common_item['name'] not in program_items_names:
                    program_items_names.append(common_item['name'])
                    program_items.append(common_item)

        data['facilities'] = facilities_shared_items
        data['facilities_names'] = facilities_shared_items_names
        data['items'] = program_items
        data['items_names'] = program_items_names
        data.pop('materials', None)
        result.append(data)
    programs_augmented = result
    print("Done augmenting programs")

def augment_facilities():
    global facilities_augmented
    print("Augmenting facilities")
    result = []
    for f in facilities:
        data = f.serialize

        facility_map[f.id] = data

        programs_shared_items = []
        programs_shared_items_names = []
        facility_items = []
        facility_items_names = []

        for material in f.materials:
            if material['id'] in program_item_map:
                # add facilities which have items in common
                common_programs = program_item_map[material['id']]
                for c in common_programs:
                    if c['name'] not in programs_shared_items_names:
                        programs_shared_items_names.append(c['name'])
                        programs_shared_items.append(c)

            if material['id'] in item_map:
                # keep track of total item list for this program
                common_item = item_map[material['id']]
                if common_item['name'] not in facility_items_names:
                    facility_items_names.append(common_item['name'])
                    facility_items.append(common_item)

        data['programs'] = programs_shared_items
        data['programs_names'] = programs_shared_items_names
        data['items'] = facility_items
        data['items_names'] = facility_items_names
        data.pop('materials', None)
        result.append(data)
    facilities_augmented = result
    print("Done augmenting facilities")

if __name__ == '__main__':
    # compute relations between models
    augment_items()
    augment_programs()
    augment_facilities()
    app.run(host="0.0.0.0")
